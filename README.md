# How to get started

## Install docker

`wget -qO- https://get.docker.com/ | sh`

## Set up the Seafile Pro and a mariaDB container

#### Build the docker image for Seafile Pro

`git clone https://bitbucket.org/chaosbunker/seafile-pro-docker.git`

`cd seafile-pro-dockerfile`

`docker build -t seafilepro .`

#### Create a docker network

`docker network create seafile-network`

#### Run the Seafile DB container (change the passwords)

	docker run  -d \
		--name=seafilepro-db \
		--restart=always \
		--network seafile-network \
		--net-alias=db \
		-v seafilepro-db-vol-1:/var/lib/mysql \
		--env MYSQL_ROOT_PASSWORD=oh-what-a-strong-password \
		--env MYSQL_USER=seafile \
		--env MYSQL_PASSWORD=another-strong-password \
	mariadb:10.1```

#### Run the Seafile Pro "Setup Container" and go through the interactive install

	docker run -it --rm \
		--name=seafilepro_setup \
		--network=seafile-network \
		-v seafilepro-data-vol-1:/seafile \
	seafilepro setup

#### Run the Seafile Pro Container

	docker run -d \
		--name=seafilepro \
		--restart=unless-stopped \
		--network=seafile-network \
		-p 8082:8082 \
		-p 8000:8000 \
		-v seafilepro-data-vol-1:/seafile \
	seafilepro

You can now access Seafile via http://\<server-ip\>:8000

#### Run Seafile behind a reverse proxy and get a certificate from [Let's Encrypt](https://letsencrypt.org) (optional but recommended)

The Seafile Pro Dockerimage is basically a fork of [xama's github repository](https://github.com/xama5/docker-seafile-pro), except that this version uses a mysql database instead of sqlite. He deserves all credit.